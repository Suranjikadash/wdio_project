const{Given,When,Then}=require('@wdio/cucumber-framework');
const samplepage =require('../page/samplepage');

Given (/^Open the browser and select textbox$/,async function(){
    await browser.url("https://artoftesting.com/samplesiteforselenium");
    await browser.maximizeWindow();   
    await samplepage.Clickinputbox();
    
   
})
When(/^the user enters value to the textbox as "Charlie"$/,async function(){
    await samplepage.Texttoinputbox("Charlie");     
    
})
Then(/^click submit$/,async function(){
    await samplepage.Submits();
})    