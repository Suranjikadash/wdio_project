const allureReporter = require('@wdio/allure-reporter').default;
const allure = require('allure-commandline');

const{Given,When,Then}=require('@wdio/cucumber-framework');
const mmtpage = require('../page/mmtpage.js');

Given (/^Open the browser and select hotel in mmt$/,async function(){
    await browser.url("https://www.makemytrip.com/");
    await browser.maximizeWindow(); 
    await mmtpage.hotels();
    await browser.pause(2000);
 })

//When(/^Selecting City$/,async function(){
    When('Selecting City {string}', async function (string) {
    await mmtpage.citynames();
    await mmtpage.Entercitys(string);
    await mmtpage.autosuggestions();
    await browser.pause(2000);

})
When(/^Selecting checkin and checkoutdate$/,async function(){
    await mmtpage.Checkins();
    await mmtpage.Checkindates();
    await mmtpage.Checkoutdates();
    await browser.pause(2000);
})
When(/^Selecting No of Persons$/,async function(){
    await mmtpage.adults();
    await mmtpage.childs();
    await mmtpage.childages();
    await mmtpage.SelectChildages("4");
    await browser.pause(2000);
})
When(/^Selecting another room$/,async function(){
    await mmtpage.Anotherrooms();
    await mmtpage.Selectadults();
    await browser.pause(2000);
})
Then(/^Select apply and serach$/,async function(){
    await mmtpage.applys();
    await mmtpage.searchs();
    await browser.pause(2000);
})
