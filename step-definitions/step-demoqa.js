const{Given,When,Then}=require('@wdio/cucumber-framework');
const demoqaPage = require('../page/demoqapage.js');
const demoqapage = require('../page/demoqapage.js');

Given (/^Open the browser and capture the title of the page$/,async function(){
    await browser.url("https://demoqa.com/browser-windows");  
    await browser.maximizeWindow();  
    console.log ("Title of the page is :"+browser.getTitle());
    console.log ("Current browser is :"+browser.getWindowHandle());
    await browser.pause(2000);
 })
 When (/^Open another window and switchback$/,async function(){
    // await browser.url("https://www.google.co.in/");
    // await browser.pause(3000);
    await browser.newWindow("https://www.amazon.in/")
    await browser.pause(9000);
    await browser.switchWindow("https://demoqa.com/browser-windows");
    await browser.pause(6000);
 })
Then (/^Selecting different windows$/,async function(){
    await demoqaPage.clicknewtab();  
    await browser.pause(2000);
    await demoqaPage.clickwindowbutton();
    // await demoqaPage.clickMessageWindow();
    await browser.pause(2000);
 }) 

