const demoqabase = require ("./demoqabase.js")
class demoqapage extends demoqabase{
    //Locators
    get newtab(){
        return  $("//button[@id='tabButton']");
    }
    get windowbutton(){
        return  $("#windowButton");
    }
    get messagewindowbutton(){
        return $("#messageWindowButton");
    }
    //Actions
    async clicknewtab(){
        await super.Doclick(this.newtab);
    }
    async clickwindowbutton(){
        await super.Doclick(this.windowbutton);
    }
    async clickMessageWindow(){
        await super.Doclick(this.messagewindowbutton);
    }

}
module.exports=new demoqapage();