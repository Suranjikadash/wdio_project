const mmtbase = require("../page/mmtbase.js");
class Mmtpage extends mmtbase{
    //locators
    get hotel(){
        return $("//span[@class='chNavIcon appendBottom2 chSprite chHotels']")
    }
    get cityname(){
        return $("//input[@id='city']")
    }
    get Entercity(){
        return $("//input[@placeholder='Enter city/ Hotel/ Area/ Building']")
    }
    get autosuggestion(){
        return $("//p[contains (text( ),'New Town, Kolkata')]")
    }
    get Checkin(){
        return $("//input[@id='checkin']")
    }
    get Checkindate(){
        return $("//div[@aria-label='Tue Dec 20 2022']")
    }
    get Checkoutdate(){
        return $("//div[@aria-label='Fri Dec 30 2022']")
    }
    get adult(){
        return $("//li[@data-cy='adults-3']")
    }
    get child(){
        return $("//li[@data-cy='children-1']")
    }
    get childage(){
        return $("//*[@id='0']") 
    } 
    get Anotherroom(){
        return $("//button[@class='btnAddRoom']")
    }
    get Selectadult (){
        return  $("//li[@data-cy='adults-1']")
    }
    get apply(){
        return $("//button[@class='primaryBtn btnApply']")
    }
    get search(){
        return $("//button[@id='hsw_search_button']")
    }
    //Actions
    async hotels(){
        await super.Doclick(this.hotel);
        
     }  
     async citynames(){
        await super.Doclick(this.cityname);
     }  
    
     async autosuggestions(){
        await super.Doclick(this.autosuggestion);
     }  
     async Checkins(){
        await super.Doclick(this.Checkin);
     } 
     async  Checkindates(){
        await super.Doclick(this. Checkindate);
     } 
     async  Checkoutdates(){
        await super.Doclick(this. Checkoutdate);
     } 
     
     async  adults(){
        await super.Doclick(this. adult);
     }
     async  childs(){
        await super.Doclick(this. child);
     }  
     async childages(){
        await super.Doclick(this.childage);
     }
     async  Anotherrooms(){
        await super.Doclick(this. Anotherroom);
     } 
     async  Selectadults(){
        await super.Doclick(this. Selectadult);
     }
     async  applys(){
        await super.Doclick(this. apply); 
     }
     async  searchs(){
        await super.Doclick(this. search);
     } 


     async Entercitys(value){
        await super.inputvalue(this.Entercity,value);
     }  
      
    
     async SelectChildages(dropdownvalue){
         await super.selectdropdown(this.childage,dropdownvalue);       
        
     }
          
         
 }
 module.exports=new Mmtpage();