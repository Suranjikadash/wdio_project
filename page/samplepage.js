const samplebase=require("../page/samplebase");
class samplepage extends samplebase{
    //locators
    get inputbox(){
        return $("//input[@id='fname']")
    }
    get submit(){
        return $("//button[@id='idOfButton']")
    }
    //Actions
    async Clickinputbox(){
        await super.Doclick(this.inputbox);
        await browser.pause(2000);

    }
    async Submits(){
        await super.Doclick(this.submit);
        await browser.pause(2000);
    }
    async Texttoinputbox(value){
        await super.Inputvalue(this.inputbox,value);
        await browser.pause(3000);
    }

} 
module.exports=new samplepage();